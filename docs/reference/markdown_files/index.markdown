# Chalgrove

Chalgrove service maintains the system preferences and handles notifications
for those preferences. 

## Overview

In its early design it was planned to use the same for apps settings.
However the same has not yet been implemented.

The requirements for Chalgrove is as follows:

* Chalgrove should read preferences and load them into DB
* Provide interface to read / write preferences
* Provide reset-factory-default interface
* On ‘App-installed’ or ‘App-removed’ refresh and reload preferences and merge prefer-ences across system-wide, per-user and per-application preferences
* Provide broadcast notifications when preferences change

## Contact

[Mail the maintainers](mailto:maintainers@lists.apertis.org)
