/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */


#ifndef __CHALGROVE_MANAGER_H__
#define __CHALGROVE_MANAGER_H__

#include <glib.h>
#include <stdio.h>
#include <gio/gio.h>
#include <glib-object.h>
#include <libxml/parser.h>
#include <glib/gstdio.h>
#include <string.h>

#include "seaton-preference.h"
#include "chalgrove.h"
#include "canterbury.h"



#define DEBUG_CHALGROVE(...)        //g_print( __VA_ARGS__)

/* Flag to enable parsers(json/xml) */
#define CHALGROVE_JSON_PARSER FALSE

#define CHALGROVE_DATATYPE    "type"
#define CHALGROVE_SUB_DATATYPE    "subtype"
#define CHALGROVE_SELECT  "select"
#define CHALGROVE_LIST    "list"
#define CHALGROVE_PERM    "perm"
#define CHALGROVE_NAME    "name"
#define CHALGROVE_APPNAME "appname"
#define CHALGROVE_VALUE   "value"
#define CHALGROVE_DEFAULT_VALUE   "defaultv"
#define CHALGROVE_STRING  "string"
#define CHALGROVE_INTEGER "integer"
#define CHALGROVE_BOOLEAN "boolean"
#define CHALGROVE_PASSWORD "password"
#define CHALGROVE_MULTILIST "multilist"

#define CHALGROVE_DYNAMIC_TABLE "DynamicTable"
#define CHALGROVE_STATIC_TABLE "StaticTable"
#define CHALGROVE_LIST_COLUMN_KEY "keyName"
#define CHALGROVE_LIST_COLUMN_VALUE "ValueSelected"

G_BEGIN_DECLS

#define CHALGROVE_TYPE_MANAGER chalgrove_manager_get_type()

#define CHALGROVE_MANAGER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  CHALGROVE_TYPE_MANAGER, ChalgroveManager))

#define CHALGROVE_MANAGER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  CHALGROVE_TYPE_MANAGER, ChalgroveManagerClass))

#define CHALGROVE_IS_MANAGER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  CHALGROVE_TYPE_MANAGER))

#define CHALGROVE_IS_MANAGER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  CHALGROVE_TYPE_MANAGER))

#define CHALGROVE_MANAGER_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  CHALGROVE_TYPE_MANAGER, ChalgroveManagerClass))

typedef struct _ChalgroveManager ChalgroveManager;
typedef struct _ChalgroveManagerClass ChalgroveManagerClass;
typedef struct _ChalgroveManagerPrivate ChalgroveManagerPrivate;

struct _ChalgroveManager
{
  GObject parent;

  ChalgroveManagerPrivate *priv;
};

struct _ChalgroveManagerClass
{
  GObjectClass parent_class;
};

GType chalgrove_manager_get_type (void) G_GNUC_CONST;

ChalgroveManager *chalgrove_manager_new (void);


typedef struct _ChalgroveData ChalgroveData;

struct _ChalgroveData
{
        gchar           *m_pAppName;
        guint           *m_uinAppId;
        SeatonPreference        *m_pPdiInstance;
        GPtrArray       *m_apPrefSet;
};

G_END_DECLS

#endif /* __CHALGROVE_MANAGER_H__ */
