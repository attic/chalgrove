/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */


#include "chalgrove-manager.h"
#include "chalgrove.h"


G_DEFINE_TYPE (ChalgroveManager, chalgrove_manager, G_TYPE_OBJECT)

#define MANAGER_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), CHALGROVE_TYPE_MANAGER, ChalgroveManagerPrivate))

struct _ChalgroveManagerPrivate
{
  gchar           *m_pDirPath;
  GList           *m_lstFullPref;
  GList           *m_prefFilePathList;
  GPtrArray       *m_garrDirContents;
  GHashTable      *m_ghs_Dynamic_Table_HashPool;
  GHashTable      *m_ghs_Static_Table_HashPool;
  GHashTable      *m_ghsHashPdi;
  GHashTable      *m_gpAvailablePrefsHashTable;
  GHashTable      *m_gpHashAppList;
  GHashTable      *m_gpHashAppListTemp;
  GList           *m_lstPdiList;
  ChalgroveService *m_pPrefMgrServiceObj;
  GVariant *app_manifest_table;
  GVariantBuilder *app_manifest_map, *pGvarient_Array_Builder;

  GVariant *out_settings_database;
  GVariant *gpAppList;
  gchar*  gcurrentLanguage;

  CanterburyPrefMgrManager *app_mgr_proxy;
};

static ChalgroveManager *m_pPrefInstance = NULL;

static CanterburyPrefMgrManager *proxy = NULL;

static void v_hash_table_iterator(gpointer key, gpointer value, gpointer user_data);
static void v_prefmgr_gdbus_connection_acquired(GDBusConnection *connection, const gchar *name, gpointer gpUserData);
static void v_prefmgr_gdbus_name_acquired(GDBusConnection *connection, const gchar *name, gpointer gpUserData);
static void v_prefmgr_gdbus_name_lost(GDBusConnection *connection, const gchar *name, gpointer gpUserData);
static gboolean gb_prefmgr_save_preference_data(ChalgroveManager* self);
static gboolean gb_prefmgr_add_data_pdi(ChalgroveManager *self, ChalgroveData *pData);
static gboolean gb_prefmgr_install_pdi(ChalgroveManager *self, ChalgroveData *pData);
static void chalgrove_parse_xml(xmlDoc *pXmlDoc, xmlNode *pXmlNode, const gchar *pSearchItem, ChalgroveData **pPrefData);
static SeatonPreference* chalgrove_get_seaton_instance(const gchar* gpAppName);
static ChalgroveManager* chalgrove_get_default_instance(void);
static GHashTable* chalgrove_variant_to_hash(GVariant* gvVar);
//static ChalgroveData* chalgrove_parse_json(const char *pFilePath, const gchar *pSearchItem);


static void
chalgrove_manager_get_property (GObject    *object,
                                guint       property_id,
                                GValue     *value,
                                GParamSpec *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
chalgrove_manager_set_property (GObject      *object,
                                guint         property_id,
                                const GValue *value,
                                GParamSpec   *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
chalgrove_manager_dispose (GObject *object)
{
  G_OBJECT_CLASS (chalgrove_manager_parent_class)->dispose (object);
}

static void
chalgrove_manager_finalize (GObject *object)
{
  G_OBJECT_CLASS (chalgrove_manager_parent_class)->finalize (object);
}

static void
chalgrove_manager_class_init (ChalgroveManagerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  g_type_class_add_private (klass, sizeof (ChalgroveManagerPrivate));

  object_class->get_property = chalgrove_manager_get_property;
  object_class->set_property = chalgrove_manager_set_property;
  object_class->dispose = chalgrove_manager_dispose;
  object_class->finalize = chalgrove_manager_finalize;
}

static void
chalgrove_manager_init (ChalgroveManager *self)
{
  self->priv = MANAGER_PRIVATE (self);
}

ChalgroveManager *
chalgrove_manager_new (void)
{
  return g_object_new (CHALGROVE_TYPE_MANAGER, NULL);
}

static SeatonPreference* gptr_get_app_instance(ChalgroveManager *self, const gchar *gpAppName)
{
    //printf("%s\n", __FUNCTION__);

    int index;

    if(!self || !gpAppName)
    {
        printf("Invalid parameter\n");
        return NULL;
    }

    ChalgroveManagerPrivate *priv = MANAGER_PRIVATE (self);

    if(priv->m_lstPdiList && g_list_length(priv->m_lstPdiList))
    {
        for(index = 0; index < g_list_length(priv->m_lstPdiList); index++)
        {
            ChalgroveData* pData = g_list_nth_data(priv->m_lstPdiList,index);

            if(pData && pData->m_pAppName && !g_strcmp0(pData->m_pAppName,gpAppName))
            {
                return pData->m_pPdiInstance;
            }
        }
    }

    return NULL;
}

/*****************************************************************
 *  chalgrove_get_seaton_instance:
 *
 *  Get PDI instance based on the application name.
 *  Creates a DB file using PDI if it doesn't exists.
 *
 *  Input : app name
 *  OutPut: pdi instance.
 *
 *******************************************************************/
SeatonPreference* chalgrove_get_seaton_instance(const gchar* gpAppName)
{
    //printf("%s\n", __FUNCTION__);

    if(!gpAppName)
    {
        printf("Invalid parameter\n");
        return NULL;
    }

    int inCount = 1;
    SeatonPreference *gpseaton = NULL;
    ChalgroveManager *self = chalgrove_get_default_instance();
    ChalgroveManagerPrivate *priv = MANAGER_PRIVATE(self);

    if(NULL == priv->m_ghsHashPdi)
    {
        /* Create a Hash table to check the created PDI instance */
        priv->m_ghsHashPdi = g_hash_table_new(g_str_hash, (GEqualFunc)g_str_equal);
        g_assert(priv->m_ghsHashPdi != NULL);
    }

    /* check for the availability of PDI instance for an application */
    if(!GPOINTER_TO_INT(g_hash_table_lookup(priv->m_ghsHashPdi, gpAppName)))
    {
        /* Create a new object, If instance is not there */
        g_hash_table_insert(priv->m_ghsHashPdi, (gchar*)gpAppName, GINT_TO_POINTER(inCount));

        ChalgroveData* pPrefData = g_new0(ChalgroveData, 1);
        g_assert(pPrefData != NULL);

        pPrefData->m_pPdiInstance = seaton_preference_new();
        pPrefData->m_pAppName = g_strdup(gpAppName);

        seaton_preference_open(pPrefData->m_pPdiInstance, "Preferences", pPrefData->m_pAppName, SEATON_PREFERENCE_SERVICE_DB);

        priv->m_lstPdiList = g_list_append(priv->m_lstPdiList, pPrefData);
        gpseaton = pPrefData->m_pPdiInstance;
    }
    else
    {
        //printf("\nGet the instance if already created\n");
        /* Get the instance if already created */
        gpseaton = gptr_get_app_instance(self, gpAppName);
        if(!gpseaton)
        {
            printf("gpseaton in NULL\n");
            return NULL;
        }

    }

    return gpseaton;
}

//start: added by shiva
static gboolean gb_prefmgr_load_factory_defaults_for_factory_reset(ChalgroveManager *self, gchar *gpPreferenceFilePath, const gchar *pAppName)
{
    //printf("%s\n", __FUNCTION__);

    ChalgroveData *pPrefData = NULL;

    if(!self || !gpPreferenceFilePath || !pAppName)
    {
        printf("Invalid parameter\n");
        return FALSE;
    }

    if(CHALGROVE_JSON_PARSER != TRUE)
    {
        g_print("########################### scanning file - %s\n",gpPreferenceFilePath);
        /* Parse xml file to xmlDoc */
        xmlDoc *pXmlDoc = xmlParseFile(gpPreferenceFilePath);

        if(pXmlDoc == NULL)
        {
            return FALSE;
        }

        /* Get xml root node element */
        xmlNode *pCurNode = xmlDocGetRootElement(pXmlDoc);
        if(pCurNode == NULL)
        {
            DEBUG_CHALGROVE("Not able to parse xml contents %s:%d\n", __FUNCTION__, __LINE__);
            return FALSE;
        }

        /* Parse "preferences" section from pxmlDoc */
        chalgrove_parse_xml(pXmlDoc, pCurNode, "preferences", &pPrefData);
    }
    else if(CHALGROVE_JSON_PARSER == TRUE)
    {
        /* Parse "preferences" section from json file */
;
        //pPrefData = chalgrove_parse_json(gpPreferenceFilePath, "preference");
    }

    /* Create the DB */
    pPrefData->m_pPdiInstance = seaton_preference_new();
    pPrefData->m_pAppName = g_strdup(pAppName);

    /* Open the DB */
    seaton_preference_open(pPrefData->m_pPdiInstance, "Preferences",  pPrefData->m_pAppName, SEATON_PREFERENCE_SERVICE_DB);

    /* PDI install */
    gb_prefmgr_install_pdi(self, pPrefData);

    /* Create PDI table */
    gb_prefmgr_add_data_pdi(self, pPrefData);

    return TRUE;
}

/*********************************************************************
 * gb_prefmgr_load_factory_defaults_for_first_boot will load the preferences from manifest file.
 * param preference manager instance
 * return result
 *****************************************************************/
static gboolean gb_prefmgr_load_factory_defaults_for_first_boot(ChalgroveManager *self)
{
    g_print("%s\n", __FUNCTION__);

    if(!self)
    {
        printf("Invalid parameter\n");
        return FALSE;
    }

    GHashTableIter iter;
    gpointer key = NULL, value = NULL;
    ChalgroveManagerPrivate *priv = MANAGER_PRIVATE(self);

    if(!priv->m_gpHashAppList)
    {
        DEBUG_CHALGROVE("\n No Preference Files %s:%d",__FUNCTION__,__LINE__);
        return FALSE;
    }

    g_hash_table_iter_init(&iter, priv->m_gpHashAppList);
    while(g_hash_table_iter_next(&iter, &key, &value))
    {
        /* here key is "app name" and value is "pref file path" */
        ChalgroveData *pPrefData = NULL;

        g_print("\n## app-name = %s path: %s\n", (char *)key,(char *)value);

#if 1
        if(CHALGROVE_JSON_PARSER != TRUE)
        {
            /* Parse xml file to xmlDoc */
            g_print("Scanning file now........\n");
            xmlDoc *pXmlDoc = xmlParseFile(value);

            if(pXmlDoc == NULL)
            {
                return FALSE;
            }

            /* Get xml root node element */
            xmlNode *pCurNode = xmlDocGetRootElement(pXmlDoc);
            if(pCurNode == NULL)
            {
                DEBUG_CHALGROVE("Not able to parse xml contents %s:%d\n", __FUNCTION__, __LINE__);
                return FALSE;
            }

            /* Parse "preferences" section from pxmlDoc */
            chalgrove_parse_xml(pXmlDoc, pCurNode, "preferences", &pPrefData);
        }
        else if(CHALGROVE_JSON_PARSER == TRUE)
#endif
        {
            ;
            /* Parse "preferences" section from json file */
//          pPrefData = chalgrove_parse_json((char *)value, "preference");
        }

        if(pPrefData != NULL)
        {
            priv->m_lstFullPref = g_list_append(priv->m_lstFullPref, pPrefData);
        }
    }

    gb_prefmgr_save_preference_data(self);

    return TRUE;
}
//End: added by shiva

/****************************************************************
 * gb_prefmgr_check_fresh_installation will check for the first time installation
 * param preference manager instance
 * return result
 *****************************************************************/
static gboolean gb_prefmgr_check_fresh_installation(ChalgroveManager *self)
{
    //printf("%s\n", __FUNCTION__);

    GDir *gpDir = NULL;
    GError *gpError = NULL;
    gboolean gbResult = FALSE;

    if(!self)
    {
        printf("Invalid parameter\n");
        return FALSE;
    }
  //const gchar* pBaseDir = g_get_home_dir();
  //gchar* pFolderPath = g_build_filename (pBaseDir, "Applications","Preferences","Storage",g_get_user_name(), NULL);
  gchar* pFolderPath = g_build_filename ("/var" , "lib" , "APERTIS_Services" ,"Preferences","app-data", NULL);
    /* Check for the files in PDI folder */
    gpDir = g_dir_open(pFolderPath, 0, &gpError);
    if(gpDir && g_dir_read_name(gpDir))
    {
        /* True if any file exists */
        gbResult = TRUE;
    }
  g_free(pFolderPath);
    return gbResult;
}

/*static void chalgrove_init(ChalgroveManager *self)
{
    //printf("%s\n", __FUNCTION__);

    if(!self)
    {
        printf("Invalid parameter\n");
        return;
    }

    self->priv = MANAGER_PRIVATE (self);

    self->priv->m_garrDirContents = NULL;
    self->priv->m_lstFullPref = NULL;
    self->priv->m_lstPdiList = NULL;
    //self->priv->m_prefFilePath = NULL;
    self->priv->m_gpHashAppList = NULL;
    self->priv->m_gpAvailablePrefsHashTable = NULL;
}*/

//Start: added by shiva
static gboolean gb_prefmgr_add_data_pdi(ChalgroveManager *self, ChalgroveData *pData)
{
    //printf("%s\n", __FUNCTION__);

    if(!self || !pData)
    {
        printf("Invalid parameter\n");
        return FALSE;
    }

    gint count;

    ChalgroveManagerPrivate *priv = MANAGER_PRIVATE(self);

    gpointer DyTableKey = NULL;
    gpointer DyTableValue = NULL;
    gpointer DataType = NULL;
    gpointer defaultValue = NULL;
    gpointer ListTableName = NULL;

    /* creata a dynamic hash table and fill (name, value) into hash table as (key, value) pair respectivly */
    GHashTable *pDynamicHashTable = g_hash_table_new(g_str_hash, (GEqualFunc)g_str_equal);
    g_assert(pDynamicHashTable != NULL);

    /* Create a hash table for Data base static table enrty. Each hash table represents one row in DB(not duplicate entry) */
    GHashTable *pStaticHashTable = g_hash_table_new(g_str_hash, (GEqualFunc)g_str_equal);
    g_assert(pStaticHashTable != NULL);

    /* Create a hash table for Data base list table enrty */
    GHashTable *pListHashTable = g_hash_table_new(g_str_hash, (GEqualFunc)g_str_equal);
    g_assert(pListHashTable != NULL);

    if(priv->m_gpAvailablePrefsHashTable == NULL)
    {
        /* Create hash table to store preference file names & there there paths */
        priv->m_gpAvailablePrefsHashTable = g_hash_table_new(g_str_hash, (GEqualFunc)g_str_equal);
        g_assert(priv->m_gpAvailablePrefsHashTable != NULL);
    }

    for(count = 0; count < pData->m_apPrefSet->len; count++)
    {
        GHashTable* pHash = (GHashTable*)g_ptr_array_index(pData->m_apPrefSet, count);

        if(pHash && g_hash_table_size (pHash))
        {
            GHashTableIter iter;
            gpointer key, value;

            // g_hash_table_foreach(pHash, (GHFunc)v_hash_table_iterator, NULL);

            /* Get hash table name, value & type */
            DyTableKey = g_hash_table_lookup(pHash, CHALGROVE_NAME);
            DyTableValue = g_hash_table_lookup(pHash, CHALGROVE_VALUE);
            DataType =  g_hash_table_lookup(pHash, CHALGROVE_DATATYPE);
            defaultValue = g_hash_table_lookup(pHash, CHALGROVE_DEFAULT_VALUE);

            /* Start: Fill data for dynamic hash table */

            /* check dynamic hash table for key availabality, if key is not avilable insert key & value into hash table.
               if the type is list or select, key will become key and value(List/Select List table name) for hash table */
            if(DyTableKey != NULL && DyTableValue != NULL)
            {
                if(!g_hash_table_lookup(pDynamicHashTable, DyTableKey))
                {
                    if(!g_strcmp0(DataType, CHALGROVE_LIST) || !g_strcmp0(DataType, CHALGROVE_SELECT) || !g_strcmp0(DataType, CHALGROVE_MULTILIST))
                    {
                        ListTableName = DyTableKey;

                        /* Add default Value for preference name */
                        if(defaultValue)
                        {
                            g_hash_table_insert(pDynamicHashTable, (gchar*)DyTableKey, (gchar*)defaultValue);
                        }
                        //g_hash_table_insert(pDynamicHashTable, (gchar*)DyTableKey, (gchar*)ListTableName);
                    }
                    else
                    {
                        g_hash_table_insert(pDynamicHashTable, (gchar*)DyTableKey, (gchar*)DyTableValue);
                    }

#if 0
                    /* Check the data type. if it is of type "preference", Get the key & value.
                       Here key is preference name & value is preference file path. */
                    if(!g_strcmp0(DataType, "preference"))
                    {
                        if(!g_hash_table_lookup(priv->m_gpAvailablePrefsHashTable, (gchar*)DyTableValue))
                        {
                            printf("######## Adding path: %s\n", (gchar*)DyTableValue);

                            g_hash_table_insert(priv->m_gpAvailablePrefsHashTable, (gchar*)DyTableValue, (gchar*)DyTableKey);

                            g_hash_table_insert(pDynamicHashTable, (gchar*)DyTableKey, (gchar*)DyTableValue);

                            //v_prefmgr_create_db_for_sub_prefernce(DyTableValue);
                        }
                    }
#endif
                }
            }
            /* End: Fill data for dynamic hash table */

            /* Start: Fill Data base static table */
            g_hash_table_iter_init(&iter, pHash);
            while(g_hash_table_iter_next(&iter, &key, &value))
            {
                /* insert below values into hash table */
                if(!g_strcmp0(key, CHALGROVE_DATATYPE) || !g_strcmp0(key, CHALGROVE_SUB_DATATYPE) || !g_strcmp0(key, CHALGROVE_PERM) || !g_strcmp0(key, CHALGROVE_NAME))
                {
                    g_hash_table_insert(pStaticHashTable, (gchar*)key, (gchar*)value);
                }

                /*if data type is "list or select", take value & default value. If value & default value are equal,
                  Value is selected otherwise value is not selected */
                if(!g_strcmp0(DataType, CHALGROVE_LIST) || !g_strcmp0(DataType, CHALGROVE_SELECT) || !g_strcmp0(DataType, CHALGROVE_MULTILIST))
                {
                    if(!g_strcmp0(key, CHALGROVE_VALUE))
                    {
                        //defaultValue = g_hash_table_lookup(pHash, DEFAULT_VALUE);

                        g_hash_table_insert(pListHashTable, CHALGROVE_LIST_COLUMN_KEY, value);

                        if(!g_strcmp0(value, defaultValue))
                        {
                            g_hash_table_insert(pListHashTable, CHALGROVE_LIST_COLUMN_VALUE, "1");
                        }
                        else
                        {
                            g_hash_table_insert(pListHashTable, CHALGROVE_LIST_COLUMN_VALUE, "0");
                        }
                    }
                }
            }

            if(g_hash_table_size(pStaticHashTable) != 0)
            {
                /* if the data type is list or select, chech the data base table CHALGROVE_NAME column for this name.
                   Insert into DB, if the entry is not available. */
                if(!g_strcmp0(DataType, CHALGROVE_LIST) || !g_strcmp0(DataType, CHALGROVE_SELECT) || !g_strcmp0(DataType, CHALGROVE_MULTILIST))
                {
                    gchar *bufferTemp = g_strdup(CHALGROVE_NAME);
                    gchar *buffer =  g_strconcat(bufferTemp, "=", "'", DyTableKey, "'", ";", NULL);

                    /* check data base table CHALGROVE_NAME column for this name entry */
                    GPtrArray *pEntryCount = seaton_preference_get_multiple(pData->m_pPdiInstance, CHALGROVE_STATIC_TABLE, CHALGROVE_NAME, buffer);

                    /* if garray size is zero, add enrty to db */
                    if(pEntryCount->len == 0)
                    {
                        /* insert into DB static table, if data type is equal to "list or select" */
                        seaton_preference_add_data(pData->m_pPdiInstance, CHALGROVE_STATIC_TABLE, pStaticHashTable);
                    }
                }
                else
                {
                    /* insert into DB static table, if data type is not equal to "list or select" */
                    seaton_preference_add_data(pData->m_pPdiInstance, CHALGROVE_STATIC_TABLE, pStaticHashTable);
                }

                /* Remove hash table entries for next fill up */
                g_hash_table_remove_all(pStaticHashTable);
            }
            /* End: Fill Data base static table */

            /* Add data to list table */
            if(g_hash_table_size(pListHashTable))
            {
                seaton_preference_add_data(pData->m_pPdiInstance, ListTableName, pListHashTable);
                /* Remove hash table entries for next fill up */
                g_hash_table_remove_all(pListHashTable);
            }
        }
    }

    /* Fill data base dynamic hash table */
    seaton_preference_add_data(pData->m_pPdiInstance, CHALGROVE_DYNAMIC_TABLE, pDynamicHashTable);

    return TRUE;
}

/*********************************************************************************
 * gb_prefmgr_create_table_columns() will create a database dynamic & static table
 * based on the preference data.
 *
 **********************************************************************************/
static gboolean gb_prefmgr_create_table_columns(ChalgroveManager *self, ChalgroveData *pData, GHashTable *hsTable, gchar *pTableName)
{
    //printf("%s\n", __FUNCTION__);

    gint columnIndex = 0;
    gpointer pkey = NULL;
    gpointer pvalue = NULL;
    GHashTableIter  iter;

    if(!self || !pData || !hsTable || !pTableName)
    {
        printf("Invalid parameter\n");
        return FALSE;
    }

    int inTableColumnLen = g_hash_table_size(hsTable);

    SeatonFieldParam pFieldAddColum[inTableColumnLen];

    g_hash_table_iter_init(&iter, hsTable);
    while( g_hash_table_iter_next(&iter, &pkey, &pvalue) )
    {
        if(pkey != NULL && pvalue != NULL)
        {
            if(!g_strcmp0(pTableName, CHALGROVE_DYNAMIC_TABLE))
            {
                /* fill each column of the database table */
                pFieldAddColum[columnIndex].pKeyName = pkey;

                if(!g_strcmp0(pvalue, CHALGROVE_STRING) || !g_strcmp0(pvalue, "preference") || !g_strcmp0(pvalue, CHALGROVE_LIST)
                        || !g_strcmp0(pvalue, CHALGROVE_SELECT) || !g_strcmp0(pvalue, CHALGROVE_PASSWORD) || !g_strcmp0(pvalue, CHALGROVE_MULTILIST))
                {
                    pFieldAddColum[columnIndex].inFieldType = SQLITE_TEXT;
                }
                else if(!g_strcmp0(pvalue, CHALGROVE_INTEGER) || !g_strcmp0(pvalue, CHALGROVE_BOOLEAN) )
                {
                    pFieldAddColum[columnIndex].inFieldType = SQLITE_INTEGER;
                }
            }
            else if(!g_strcmp0(pTableName, CHALGROVE_STATIC_TABLE))
            {
                pFieldAddColum[columnIndex].pKeyName = pkey;
                pFieldAddColum[columnIndex].inFieldType = SQLITE_TEXT;
            }

            columnIndex++;
        }
    }

    /* create a database table based on table name(dynamic or static) */
     seaton_preference_install(pData->m_pPdiInstance, pTableName, pFieldAddColum, inTableColumnLen);

    return TRUE;
}


static gboolean gb_prefmgr_create_list_table_columns(ChalgroveManager *self, ChalgroveData *pData, gchar *pTableName)
{
    //printf("%s\n", __FUNCTION__);

    SeatonFieldParam pFieldAddColum[2];

    if(!self || !pData || !pTableName)
    {
        printf("Invalid parameter\n");
        return FALSE;
    }

    gchar *pListTableNameTemp = g_strdup(pTableName);

    /* create a list table of these column */
    pFieldAddColum[0].pKeyName = CHALGROVE_LIST_COLUMN_KEY;
    pFieldAddColum[0].inFieldType = SQLITE_TEXT;
    pFieldAddColum[1].pKeyName = CHALGROVE_LIST_COLUMN_VALUE;
    pFieldAddColum[1].inFieldType = SQLITE_INTEGER;

    /* call pdi to create a 2 column list table */
     seaton_preference_install(pData->m_pPdiInstance, pListTableNameTemp, pFieldAddColum, 2);

    return TRUE;
}


/*******************************************************************************
 * gb_prefmgr_install_pdi() will collect values required to build dynamic & static database tables clomuns
 * and also call create columns API to do an action.
 **********************************************************************************/
static gboolean gb_prefmgr_install_pdi(ChalgroveManager *self, ChalgroveData *pData)
{
    //printf("%s\n", __FUNCTION__);

    if(!self || !pData)
    {
        printf("Invalid parameter\n");
        return FALSE;
    }

    gint index;
    ChalgroveManagerPrivate* priv = MANAGER_PRIVATE (self);

    priv->m_ghs_Dynamic_Table_HashPool = NULL;
    priv->m_ghs_Static_Table_HashPool = NULL;

    for(index = 0; index < pData->m_apPrefSet->len; index++)
    {
        gint hashIndex;
        GHashTableIter  iter;
        gpointer ikey = NULL;
        gpointer ivalue = NULL;
        gpointer dataType = NULL;

        /* Get the hash table from array */
        GHashTable *hsTable = g_ptr_array_index(pData->m_apPrefSet, index);

        if(hsTable && g_hash_table_size(hsTable))
        {
            /* parse hash table for key-value pair */
            g_hash_table_iter_init(&iter, hsTable);
            for(hashIndex =0; hashIndex < g_hash_table_size(hsTable); hashIndex++)
            {
                g_hash_table_iter_next(&iter, &ikey, &ivalue);

                if(ikey != NULL && ivalue != NULL)
                {
                    /* pick values for xml attribute CHALGROVE_NAME */
                    if(!g_strcmp0(ikey, CHALGROVE_NAME))
                    {
                        /* create a dynamic hash table pool */
                        if(priv->m_ghs_Dynamic_Table_HashPool == NULL)
                        {
                            priv->m_ghs_Dynamic_Table_HashPool = g_hash_table_new(g_str_hash, (GEqualFunc)g_str_equal);
                            g_assert(priv->m_ghs_Dynamic_Table_HashPool != NULL);
                        }

                        if(!GPOINTER_TO_INT(g_hash_table_lookup(priv->m_ghs_Dynamic_Table_HashPool, ivalue)))
                        {
                            /* take the corresponding data type */
                            dataType = g_hash_table_lookup(hsTable, CHALGROVE_DATATYPE);

                            /* if type is list or select or multilist, create a list table */
                            if(!g_strcmp0(dataType, CHALGROVE_LIST) || !g_strcmp0(dataType, CHALGROVE_SELECT) || !g_strcmp0(dataType, CHALGROVE_MULTILIST))
                            {
                                gb_prefmgr_create_list_table_columns(self, pData, ivalue);
                            }

                            /* add CHALGROVE_NAME into dynamic hash table */
                            g_hash_table_insert(priv->m_ghs_Dynamic_Table_HashPool, ivalue, dataType);

                            /* add CHALGROVE_NAME into static hash table */
                            g_hash_table_insert(priv->m_ghs_Static_Table_HashPool, ikey, dataType);
                        }
                    }
                    else if(!g_strcmp0(ikey, CHALGROVE_DATATYPE) || !g_strcmp0(ikey, CHALGROVE_SUB_DATATYPE) || !g_strcmp0(ikey, CHALGROVE_PERM))
                    {
                        /* create a static hash table pool */
                        if(priv->m_ghs_Static_Table_HashPool == NULL)
                        {
                            priv->m_ghs_Static_Table_HashPool = g_hash_table_new(g_str_hash, (GEqualFunc)g_str_equal);
                            g_assert(priv->m_ghs_Static_Table_HashPool != NULL);
                        }

                        /* check static table for any entry already available, if not add an entry */
                        if(!GPOINTER_TO_INT(g_hash_table_lookup (priv->m_ghs_Static_Table_HashPool, ikey)))
                        {
                            dataType = g_hash_table_lookup (hsTable, CHALGROVE_DATATYPE);
                            g_hash_table_insert(priv->m_ghs_Static_Table_HashPool, ikey, dataType);
                        }
                    }
                }
            }
        }
    }

    /* Create a dynamic table */
    gb_prefmgr_create_table_columns(self, pData, priv->m_ghs_Dynamic_Table_HashPool, CHALGROVE_DYNAMIC_TABLE);

    /* Create a static table */
    gb_prefmgr_create_table_columns(self, pData, priv->m_ghs_Static_Table_HashPool, CHALGROVE_STATIC_TABLE);

    return TRUE;
}
//end: added by shiva

/****************************************************************
 * gb_prefmgr_save_preference_data will call store and create table api's based on the
 * preferences read from the manifest file.
 * param preference manager instance
 * return result
 *****************************************************************/
static gboolean gb_prefmgr_save_preference_data(ChalgroveManager* self)
{
    //printf("%s\n", __FUNCTION__);

    if(!self)
    {
        printf("Invalid parameter\n");
        return FALSE;
    }

    ChalgroveManagerPrivate* priv = MANAGER_PRIVATE (self);

    int index;

    if(priv->m_lstFullPref && g_list_length(priv->m_lstFullPref))
    {
        GList* pList = priv->m_lstFullPref;

        for(index = 0; index < g_list_length(pList); index++)
        {
            ChalgroveData* pPrefData = g_list_nth_data (pList, index);

            if(!pPrefData || !pPrefData->m_apPrefSet || !pPrefData->m_pPdiInstance)
            {
                return FALSE;
            }

            /* PDI install */
            gb_prefmgr_install_pdi(self, pPrefData);

            /* Create PDI table */
            gb_prefmgr_add_data_pdi(self, pPrefData);
        }
    }

    return TRUE;
}

#if 10
/* start: New from shiva */

gboolean app_settings_changed_clb(CanterburyPrefMgrManager *object, const gchar *arg_app_name, guint arg_app_state, GVariant *arg_arguments)
{
    g_print("\napp_settings_changed_clb\n app state is %d\n ", arg_app_state);

    return TRUE;
}

static void v_get_app_list_to_display(GVariant *gpAppManagerList)
{
    //printf("%s\n", __FUNCTION__);

    if(!gpAppManagerList)
    {
        printf("Invalid parameter\n");
        return;
    }

    GVariantIter iter;
    gchar *pValue = NULL, *pKey = NULL;

    ChalgroveManager *self = chalgrove_get_default_instance();
    ChalgroveManagerPrivate* priv = MANAGER_PRIVATE(self);

    GVariantBuilder *gvb = g_variant_builder_new(G_VARIANT_TYPE("a{ss}"));

    if(!priv->m_gpHashAppList)
    {
        priv->m_gpHashAppList = g_hash_table_new(g_str_hash, (GEqualFunc)g_str_equal);
        g_assert(priv->m_gpHashAppList != NULL);
    }

    g_variant_iter_init(&iter, gpAppManagerList);
    while(g_variant_iter_next(&iter, "{ss}", &pKey, &pValue))
    {
        printf("key '%s' has value '%s'\n", pKey, pValue);

        /* Add this hash table entry for app name & there path: Example, this is used in factory reset to get app name & file path */
        g_hash_table_insert(priv->m_gpHashAppList, (gchar*)pKey, (gchar*)pValue);

        g_variant_builder_add(gvb, "{ss}", pKey, pValue);
    }

    priv->gpAppList = g_variant_builder_end(gvb);
    g_variant_builder_unref(gvb);
}

gchar* language = NULL;
void language_list( gpointer key,
                    gpointer value,
                    gpointer user_data)
{
  ChalgroveManager *self = chalgrove_get_default_instance();
    ChalgroveManagerPrivate *priv = MANAGER_PRIVATE(self);

    if( FALSE == g_strcmp0( "keyName" , (gchar*)key ) )
    {
      language = g_strdup((gchar*)value);
     // printf("%s\n", language);
    }

  if( FALSE == g_strcmp0( "ValueSelected" , (gchar*)key ) )
  {
    if( FALSE == g_strcmp0( "1" , (gchar*)value ) )
    {
       priv->gcurrentLanguage = language;
      //g_print(" key %s val %s \n", (gchar*)priv->gcurrentLanguage , (gchar*)value );
      if(priv->m_pPrefMgrServiceObj)
      {
        g_print(" set new language %s \n", (gchar*)priv->gcurrentLanguage);
         chalgrove_service_set_language(priv->m_pPrefMgrServiceObj , priv->gcurrentLanguage);
      }
    }
  }
}

gboolean set_current_language()
{
    GPtrArray *gaPref = NULL;
    SeatonPreference* pdi = NULL;

  pdi = chalgrove_get_seaton_instance("mildenhall-settings");

  if(pdi == NULL)
  {
    g_print("pdi is null\n");
    return FALSE;
  }


    gaPref = seaton_preference_get_multiple(pdi, (gchar *) "LanguageList", NULL, NULL);

    if(gaPref->len == 0)
    {
        g_print("gpointer array lenght is 0\n");
        return FALSE;
    }

    GHashTable *pHash = NULL;

    int count;

    if(gaPref->len > 0)
    {
        for(count = 0; count < gaPref->len; count++)
        {
            pHash = g_ptr_array_index (gaPref, count);

            if((pHash != NULL))
            {
              g_hash_table_foreach(pHash , language_list , NULL);
            }
        }
    }
    return TRUE;
}

void get_app_settings_list(GObject *source_object, GAsyncResult *res, gpointer user_data)
{
    //printf("%s\n", __FUNCTION__);

    GError *error = NULL;

    ChalgroveManager *self = chalgrove_get_default_instance();
    ChalgroveManagerPrivate* priv = MANAGER_PRIVATE(self);

    priv->gpAppList = NULL;

    canterbury_pref_mgr_manager_call_get_settings_database_list_finish(proxy, &(priv->out_settings_database), res, &error);

    if(!priv->out_settings_database)
    {
        return;
    }

    v_get_app_list_to_display(priv->out_settings_database);

    /* Load Preferences */
    if(!gb_prefmgr_check_fresh_installation(self))
    {
        gb_prefmgr_load_factory_defaults_for_first_boot(self);
    }
    /* set language property */
    set_current_language();

    g_bus_own_name(G_BUS_TYPE_SESSION,              // bus type
        "org.apertis.Chalgrove.Service",     // interface name
        G_BUS_NAME_OWNER_FLAGS_NONE,        // bus own flag, can be used to take away the bus and give it to another service
        v_prefmgr_gdbus_connection_acquired,    // callback invoked when the bus is acquired
        v_prefmgr_gdbus_name_acquired,      // callback invoked when interface name is acquired
        v_prefmgr_gdbus_name_lost,          // callback invoked when name is lost to another service or other reason
        NULL,               // user data
        NULL);          // user data free func
}

void app_mgr_proxy_clb(GObject *source_object, GAsyncResult *res, gpointer user_data)
{
    //printf("%s\n", __FUNCTION__);

    GError *error = NULL;
    GCancellable *cancellable = NULL;

    /* finishes the proxy creation and gets the proxy ptr */
    proxy = canterbury_pref_mgr_manager_proxy_new_finish(res, &error);

    if(proxy == NULL)
    {
        g_print("error %s \n", g_dbus_error_get_remote_error(error));
    }

    if(proxy != NULL)
    {
        g_signal_connect(proxy, "app-settings-database-update", (GCallback)app_settings_changed_clb, user_data);
    }

    canterbury_pref_mgr_manager_call_get_settings_database_list(proxy, cancellable, get_app_settings_list, user_data);
}

static void v_app_mgr_name_appeared(GDBusConnection *connection, const gchar *name, const gchar *name_owner, gpointer user_data)
{
    //printf("%s\n", __FUNCTION__);

    /* Asynchronously creates a proxy for the App manager D-Bus interface */
  canterbury_pref_mgr_manager_proxy_new(connection,
            G_DBUS_PROXY_FLAGS_NONE,
            "org.apertis.Canterbury",
            "/org/apertis/Canterbury/PrefMgrManager",
            NULL,
            app_mgr_proxy_clb,
            user_data);
}

static void v_app_mgr_name_vanished(GDBusConnection *connection, const gchar *name, gpointer user_data)
{
    //printf("%s\n", __FUNCTION__);

    ChalgroveManager *self = chalgrove_get_default_instance();
    ChalgroveManagerPrivate* priv = MANAGER_PRIVATE (self);

    if(NULL != priv->app_mgr_proxy)
    {
        g_object_unref(priv->app_mgr_proxy);
    }
}
/* End: New from shiva */
#endif

static gboolean chalgrove_setmain( )
{
    //printf("%s\n", __FUNCTION__);

    /* Start: New from shiva */
    g_bus_watch_name(G_BUS_TYPE_SESSION,
            "org.apertis.Canterbury",
            G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
            v_app_mgr_name_appeared,
            v_app_mgr_name_vanished,
            NULL,
            NULL);
    /* End: New from shiva */

    return 0;
}

ChalgroveManager* chalgrove_get_default_instance(void)
{
    //printf("%s\n", __FUNCTION__);

    if(!m_pPrefInstance)
    {
        m_pPrefInstance = g_object_new(CHALGROVE_TYPE_MANAGER, NULL);

        if(!m_pPrefInstance)
        {
            return NULL;
        }
    }

    return m_pPrefInstance;
}

//Start: added by shiva
static void v_hash_table_iterator(gpointer key, gpointer value, gpointer user_data)
{
    //printf("user_data: %s & %s\n", (char *)key, (char *)value);

    if(key == NULL)
    {
        key = " ";
    }

    if(value == NULL)
    {
        value = " ";
    }

    /* Add key & value to gvarient array */
    if(user_data != NULL)
    {
        g_variant_builder_add((GVariantBuilder *)user_data, "{ss}", key, value);
    }
}

static GVariant* gp_convert_array_hash_to_varient_array(GHashTable *pHash)
{
    //printf("%s\n", __FUNCTION__);

    if(!pHash)
    {
        printf("Invalid parameters\n");
        return NULL;
    }

    GVariant *pGvarient_Array = NULL;

    GVariantBuilder *pGvarient_Array_Builder = g_variant_builder_new(G_VARIANT_TYPE ("a{ss}"));

    if((pHash != NULL))
    {
        g_hash_table_foreach(pHash, (GHFunc)v_hash_table_iterator, pGvarient_Array_Builder);
    }

    pGvarient_Array = g_variant_builder_end(pGvarient_Array_Builder);
    g_variant_builder_unref(pGvarient_Array_Builder);

    return pGvarient_Array;
}


#if 10
static GVariant* gp_get_array_of_hash(GPtrArray *gaPref)
{
    //printf("%s\n", __FUNCTION__);

    GVariant *pHash =NULL;
    GVariant *pArray_hash =NULL;
    GHashTable *hsTable = NULL;
    int count;

    if(gaPref->len > 0)
    {
        GVariantBuilder *pGvb = g_variant_builder_new(G_VARIANT_TYPE_ARRAY );
        g_variant_builder_init(pGvb, G_VARIANT_TYPE_ARRAY);

        for(count = 0; count < gaPref->len; count++)
        {
            hsTable = g_ptr_array_index(gaPref, count);

            if((hsTable != NULL))
            {
                pHash = gp_convert_array_hash_to_varient_array(hsTable);
                if(!pHash)
                {
                    printf("GVariant is NULL\n");
                    return NULL;
                }
            }

            if((pHash != NULL) && (pGvb != NULL))
            {
                g_variant_builder_add_value(pGvb, pHash);
            }
        }

        pArray_hash = g_variant_builder_end(pGvb);
        g_variant_builder_unref(pGvb);
    }

    return pArray_hash;
}
#endif

#if 0
static GVariant* gp_convert_array_hash_to_varient_array(GPtrArray *gaPref)
{

    GVariant *pGvarient_Array = NULL;
    GHashTable *pHash = NULL;

    ChalgroveManager *self = chalgrove_get_default_instance();
    ChalgroveManagerPrivate *priv = MANAGER_PRIVATE(self);

    int count;

    if(gaPref->len > 0)
    {
        priv->pGvarient_Array_Builder = g_variant_builder_new(G_VARIANT_TYPE_ARRAY);

        for(count = 0; count < gaPref->len; count++)
        {
            pHash = g_ptr_array_index (gaPref, count);

            if((pHash != NULL) && (priv->pGvarient_Array_Builder != NULL))
            {
                g_hash_table_foreach(pHash, (GHFunc)v_hash_table_iterator, priv->pGvarient_Array_Builder);
            }
        }

        pGvarient_Array = g_variant_builder_end (priv->pGvarient_Array_Builder);
        g_variant_builder_unref(priv->pGvarient_Array_Builder);
    }

    return pGvarient_Array;
}
//End: added by shiva
#endif

/*******************************************************************
 * handle_add_preferences will add the preferences
 * param preference manager service object
 * param dbus invocation
 * param appName
 * param values - in variant format
 * param conditions
 * return result
 ********************************************************************/
gboolean handle_add_preferences(ChalgroveService *object, GDBusMethodInvocation *invocation, const gchar *gpAppName,
        GVariant *gvValue, const gchar *gpCond, const gchar *pTableName)
{
    //printf("%s\n", __FUNCTION__);

    gboolean bResult = TRUE;

    /* Convert variant to hash */
    GHashTable *pHash = chalgrove_variant_to_hash(gvValue);

    /* Get the pdi instance based on the appname */
    SeatonPreference *pdi = chalgrove_get_seaton_instance(gpAppName);

    if(!pHash || !pdi)
    {
        DEBUG_CHALGROVE("\n No Data To ADD %s %d \n",__FUNCTION__,__LINE__);
        return FALSE;
    }

    if(seaton_preference_add_data(pdi, (gchar *)pTableName, pHash) != SEATON_PREFERENCE_PM_OK)
    {
        DEBUG_CHALGROVE("in_PDI_AddData is failed\n");
    }

    chalgrove_service_complete_add_preferences(object, invocation, bResult);

    return TRUE;
}

/*******************************************************************
 * handle_set_preferences will set the preferences
 * param preference manager service object
 * param dbus invocation
 * param appName
 * param keyname
 * param values
 * param conditions
 * return result
 ********************************************************************/
gboolean handle_set_preferences(ChalgroveService *object, GDBusMethodInvocation *invocation, const gchar *pAppName,
        const gchar *pUniqueKey, const gchar *pUniqueValue, const gchar *pData,  const gchar *pKeyStr, const gchar *pTableName)
{
    //printf("%s\n", __FUNCTION__);

    gboolean bResult = TRUE;
    SeatonPreference *pdi = NULL;
    gchar **pHash_Table_Array = NULL;

    ChalgroveManager *self = chalgrove_get_default_instance ();
    ChalgroveManagerPrivate *priv = MANAGER_PRIVATE (self);

    if(pAppName != NULL)
    {
        pdi = chalgrove_get_seaton_instance(pAppName);

        if(pdi == NULL)
        {
            g_print("pdi is null\n");
            return FALSE;
        }
    }

//  printf("### pTableName: %s\tpUniqueKey: %s\tpUniqueValue: %s\tpKeyStr: %s\tpData: %s\tapp-name: %s\n",
    //      pTableName, pUniqueKey, pUniqueValue, pKeyStr, pData, pAppName);

    if(!g_strcmp0(pTableName, "NULL"))
    {
        pTableName = NULL;
    }

    if(!g_strcmp0(pUniqueKey, "NULL"))
    {
        pUniqueKey = NULL;
    }

    if(!g_strcmp0(pUniqueValue, "NULL"))
    {
        pUniqueValue = NULL;
    }

    if((seaton_preference_set(pdi, (gchar *)pTableName, (gchar *)pUniqueKey, (gchar*) pUniqueValue, pKeyStr, (gchar*)pData)) == SEATON_PREFERENCE_PM_OK)
    {
        pHash_Table_Array = g_new0(gchar *, 7);
        pHash_Table_Array[0] = g_strdup(pTableName);
        pHash_Table_Array[1] = g_strdup(pUniqueKey);
        pHash_Table_Array[2] = g_strdup(pUniqueValue);
        pHash_Table_Array[3] = g_strdup(pKeyStr);
        pHash_Table_Array[4] = g_strdup(pData);
        pHash_Table_Array[5] = g_strdup(pAppName);
        pHash_Table_Array[6] = NULL;
    }

  if( ( FALSE == g_strcmp0(pTableName, "LanguageList")) && ( FALSE == g_strcmp0(pData,"1") ) )
    set_current_language();

    chalgrove_service_complete_set_preferences(object, invocation, bResult);

    /* Send notifications to applications */
    if(pHash_Table_Array != NULL)
    {
        chalgrove_service_emit_contents_changed (priv->m_pPrefMgrServiceObj, pAppName, (const gchar * const*)pHash_Table_Array);
    }

    return TRUE;
}

/*******************************************************************
 * handle_get_preferences will get the preferences
 * param preference manager service object
 * param dbus invocation
 * param appName
 * param keyname
 * param conditions
 * return result
 ********************************************************************/
gboolean handle_get_preferences(ChalgroveService *object, GDBusMethodInvocation *invocation, const gchar *pAppName,
        const gchar *pKey, const gchar *pCond, const gchar *pTableName)
{
    //printf("%s\n", __FUNCTION__);

    GPtrArray *gaPref = NULL;
    SeatonPreference* pdi = NULL;
    GVariant *pGavarient_Array_Data = NULL;

    if(pAppName != NULL)
    {
        pdi = chalgrove_get_seaton_instance(pAppName);

        if(pdi == NULL)
        {
            g_print("pdi is null\n");
            return FALSE;
        }
    }

    if((!g_strcmp0(pKey, "NULL")))
    {
        pKey = NULL;
    }

    if(!g_strcmp0(pCond, "NULL"))
    {
        pCond = NULL;
    }

    gaPref = seaton_preference_get_multiple(pdi, (gchar *) pTableName, (gchar *)pKey, (gchar*)pCond);

    if(gaPref->len == 0)
    {
        g_print("gpointer array lenght is 0\n");
        return FALSE;
    }

    if(gaPref != NULL)
    {
        pGavarient_Array_Data = gp_get_array_of_hash(gaPref);
        //pGavarient_Array_Data = gp_convert_array_hash_to_varient_array(gaPref);

        if(pGavarient_Array_Data == NULL)
        {
            g_print("Gvarient array varaible is null\n");
            return FALSE;
        }
    }

    /* Get the DB entries in gvarient array */
    chalgrove_service_complete_get_preferences(object, invocation, pGavarient_Array_Data);

    return TRUE;
}

/*******************************************************************
 * handle_remove_preferences will remove the preferences
 * param preference manager service object
 * param dbus invocation
 * param appName
 * param conditions
 * return result
 ********************************************************************/
gboolean handle_remove_preferences(ChalgroveService *object, GDBusMethodInvocation *invocation, const gchar *pAppName,
        const gchar *gpKey, const gchar *gpValue, const gchar *pTableName)
{
    //printf("%s\n", __FUNCTION__);

    gboolean bResult = TRUE;

    SeatonPreference* pdi = chalgrove_get_seaton_instance(pAppName);

    seaton_preference_remove(pdi, (gchar *)pTableName, (gchar *)gpKey, (gchar *)gpValue);

    chalgrove_service_complete_remove_preferences(object, invocation,bResult);

    return TRUE;
}

/*******************************************************************
 * handle_factory_reset will reset to defaults
 * param preference manager service object
 * param dbus invocation
 * param appName
 * return result
 ********************************************************************/
gboolean handle_factory_reset(ChalgroveService *object, GDBusMethodInvocation *invocation, const gchar *pAppName)
{
    //printf("%s\n", __FUNCTION__);

    gboolean bResult = TRUE;

    /* Delete specified DB file name present in "~/pdi" &
     * create new DB's for that application available from the app manager pref file path */

    ChalgroveManager *self = chalgrove_get_default_instance();
    ChalgroveManagerPrivate *priv = MANAGER_PRIVATE(self);

    gpointer gPrefFilePath = NULL;

    /*Get the file path for the given app name*/
    if(priv->m_gpHashAppList != NULL)
    {
        /*Get the Pref file path for given app-name*/
        gPrefFilePath = g_hash_table_lookup(priv->m_gpHashAppList, pAppName);
//    const gchar* pBaseDir = g_get_home_dir();
    //gchar* pFolderPath = g_build_filename (pBaseDir, "Applications","Preferences","Storage",g_get_user_name(), NULL);
    gchar* pFolderPath = g_build_filename ("/Applications","Preferences","Storage",g_get_user_name(), NULL);
        gchar *gDbFileName = g_strconcat(pFolderPath, "/", pAppName, ".db", NULL);
    g_free(pFolderPath);
        /*Given app name path is found*/
        if(gDbFileName != NULL)
        {
            /*Remove this DB from pdi directory*/
            if(g_remove((const gchar *)gDbFileName) == -1)
            {
                DEBUG_CHALGROVE("\ng_remove is failed\n");
                return FALSE;
            }

            /*Create new DB file from corresponding default preference file*/
            if(gPrefFilePath != NULL)
            {
                if(gb_prefmgr_load_factory_defaults_for_factory_reset(self, gPrefFilePath, pAppName) != TRUE)
                {
                    DEBUG_CHALGROVE("\nfailed in gb_prefmgr_load_factory_defaults_for_factory_reset\n");
                    return FALSE;
                }
            }

            chalgrove_service_complete_factory_reset(object, invocation, bResult);
        }
    }

    return TRUE;
}

gboolean handle_application_list(ChalgroveService *object, GDBusMethodInvocation *invocation, const gchar *pAppName)
{
    //printf("%s\n", __FUNCTION__);

    ChalgroveManager *self = chalgrove_get_default_instance();
    ChalgroveManagerPrivate *priv = MANAGER_PRIVATE(self);

    v_get_app_list_to_display(priv->out_settings_database);

    if(priv->gpAppList != NULL)
    {
        chalgrove_service_complete_application_list(object, invocation, priv->gpAppList);
    }

    return TRUE;
}

static void v_prefmgr_gdbus_connection_acquired(GDBusConnection *gpConnection, const gchar *gpName, gpointer gpUserData)
{
    //printf("%s\n", __FUNCTION__);

    ChalgroveManager *self = chalgrove_get_default_instance();
    ChalgroveManagerPrivate *priv = MANAGER_PRIVATE(self);

    priv->m_pPrefMgrServiceObj = chalgrove_service_skeleton_new();
   chalgrove_service_set_language(priv->m_pPrefMgrServiceObj , priv->gcurrentLanguage);

    g_signal_connect(priv->m_pPrefMgrServiceObj, "handle-add-preferences", G_CALLBACK(handle_add_preferences), gpUserData);
    g_signal_connect(priv->m_pPrefMgrServiceObj, "handle-set-preferences", G_CALLBACK(handle_set_preferences), gpUserData);
    g_signal_connect(priv->m_pPrefMgrServiceObj, "handle-get-preferences", G_CALLBACK(handle_get_preferences), gpUserData);
    g_signal_connect(priv->m_pPrefMgrServiceObj, "handle-remove-preferences", G_CALLBACK(handle_remove_preferences), gpUserData);
    g_signal_connect(priv->m_pPrefMgrServiceObj, "handle-factory-reset", G_CALLBACK(handle_factory_reset), gpUserData);
    g_signal_connect(priv->m_pPrefMgrServiceObj, "handle-application-list", G_CALLBACK(handle_application_list), gpUserData);

    if (!g_dbus_interface_skeleton_export(G_DBUS_INTERFACE_SKELETON(priv->m_pPrefMgrServiceObj), gpConnection, "/org/apertis/Chalgrove/Service", NULL))
    {
        DEBUG_CHALGROVE("\n export error \n");
    }
}

static void v_prefmgr_gdbus_name_acquired(GDBusConnection *connection, const gchar *name, gpointer gpUserData)
{
    DEBUG_CHALGROVE("\n name acquired\n");
}

static void v_prefmgr_gdbus_name_lost(GDBusConnection *connection, const gchar *name, gpointer gpUserData)
{
    DEBUG_CHALGROVE("\n name lost\n");
}

GHashTable* chalgrove_variant_to_hash(GVariant* gvVar)
{
    //printf("%s\n", __FUNCTION__);

    GVariantIter iter;
    gchar *pValue;
    gchar *pKey;

    if(!gvVar)
    {
        return NULL;
    }

    GHashTable* pHash = g_hash_table_new(g_str_hash, (GEqualFunc)g_str_equal);

    g_variant_iter_init(&iter, gvVar);

    while(g_variant_iter_next(&iter, "{ss}", &pKey, &pValue))
    {
        g_hash_table_insert(pHash, (gchar*)pKey, (gchar*)pValue);

        DEBUG_CHALGROVE("key '%s' has value '%s'\n", pKey, pValue);
    }

    return pHash;
}

static void chalgrove_parse_xml(xmlDoc *pXmlDoc, xmlNode *pXmlNode, const gchar *pSearchItem, ChalgroveData **pPrefData)
{
    static gboolean     bFirst = FALSE;
    xmlNode         *pCurXmlNode = NULL;
    static gchar        *pParentNode = NULL;

    if(!pSearchItem || !pXmlNode || !pXmlDoc)
    {
        return;
    }

    for(pCurXmlNode = pXmlNode; pCurXmlNode; pCurXmlNode = pCurXmlNode->next)
    {
        g_print("**************************** node name - %s\n", pCurXmlNode->name);
        if(pCurXmlNode->type == XML_ELEMENT_NODE)
        {
            /* Get application name */
            /* TODO: get application name from application manager:
               Pref manager should register to application manager for getting notifications on installation & removel of app */

            if(!xmlStrcmp(pCurXmlNode->name, (const xmlChar *)CHALGROVE_APPNAME))
            {
                if(NULL == (*pPrefData))
                {
                    (*pPrefData) = g_new0(ChalgroveData, 1);
                    g_assert((*pPrefData) != NULL);

                    (*pPrefData)->m_apPrefSet = NULL;
                    (*pPrefData)->m_pAppName = NULL;
                    (*pPrefData)->m_pPdiInstance = NULL;

                    (*pPrefData)->m_pAppName = (gchar*) xmlNodeListGetString(pXmlDoc, pCurXmlNode->xmlChildrenNode, 1);

                    /* Get the PDI instance*/
                    (*pPrefData)->m_pPdiInstance = chalgrove_get_seaton_instance((*pPrefData)->m_pAppName);
                }
            }

            /* Check for the Item to be searched */
            if(!xmlStrcmp(pCurXmlNode->name, (const xmlChar*)pSearchItem))
            {
                /* Get the parent name to get start & end of xml item to be searched */
                if(pCurXmlNode->parent)
                {
                    pParentNode= g_strdup((const gchar*)pCurXmlNode->parent->name);
                }

            }
            else if(pParentNode && pCurXmlNode->parent && xmlStrcmp(pCurXmlNode->parent->name, (const xmlChar*)pParentNode))
            {

                /* Check for the properties */
                if(pCurXmlNode->properties)
                {
                    /* Extract all the attributes available within the node and prepare a hash table. */
                    xmlAttrPtr pXmlAttr;

                    GHashTable *pHashNew = g_hash_table_new (g_str_hash, (GEqualFunc)g_str_equal);
                    g_assert(pHashNew != NULL);

                    for(pXmlAttr = pCurXmlNode->properties; NULL != pXmlAttr; pXmlAttr = pXmlAttr->next)
                    {
                        gchar *pKey = (gchar*)pXmlAttr->name;
                        gchar *pValue = (gchar*)xmlGetProp(pCurXmlNode, (const xmlChar*)pXmlAttr->name);

                        g_hash_table_insert(pHashNew, pKey, pValue);
                    }
                    /* Create a handle for the array of hash tables */
                    if(NULL == (*pPrefData)->m_apPrefSet)
                    {
                        (*pPrefData)->m_apPrefSet = g_ptr_array_new();
                    }
                    g_ptr_array_add((*pPrefData)->m_apPrefSet, (gpointer)pHashNew);

                    bFirst = FALSE;
                }
                else if((*pPrefData) && (*pPrefData)->m_apPrefSet)
                {
                    /*Get all the elements within the node (values) and store in hash table.*/
                    GHashTable *pHashLast = (GHashTable*)g_ptr_array_index((*pPrefData)->m_apPrefSet, ((*pPrefData)->m_apPrefSet->len)-1);

                    gchar *strContent = (gchar*) xmlNodeListGetString(pXmlDoc, pCurXmlNode->xmlChildrenNode, 1);

                    if(pHashLast && g_hash_table_size(pHashLast))
                    {
                        /* key with multiple values*/
                        if(!g_strcmp0((const gchar*)g_hash_table_lookup(pHashLast, CHALGROVE_DATATYPE), CHALGROVE_SELECT) ||
                                !g_strcmp0((const gchar*)g_hash_table_lookup(pHashLast, CHALGROVE_DATATYPE), CHALGROVE_LIST) ||
                                !g_strcmp0((const gchar*)g_hash_table_lookup(pHashLast, CHALGROVE_DATATYPE), CHALGROVE_MULTILIST))
                        {
                            gint index;
                            GHashTableIter iter;
                            gpointer ikey, ivalue;

                            if(!bFirst)
                            {
                                g_hash_table_insert(pHashLast,  (gpointer)pCurXmlNode->name,strContent);

                                bFirst = TRUE;
                            }
                            else
                            {
                                /*create a hash with a copy of previous instance, only the values being changed */
                                GHashTable *pHash = g_hash_table_new(g_str_hash, (GEqualFunc)g_str_equal);

                                g_hash_table_iter_init (&iter, pHashLast);
                                for(index =0; index < g_hash_table_size (pHashLast); index++)
                                {
                                    g_hash_table_iter_next(&iter, &ikey, &ivalue);

                                    if(ikey && ivalue)
                                    {
                                        g_hash_table_insert(pHash, g_strdup((const gchar*)ikey), g_strdup((const gchar*)ivalue));
                                    }
                                }

                                g_hash_table_insert(pHash, (gpointer)pCurXmlNode->name, strContent);

                                g_ptr_array_add((*pPrefData)->m_apPrefSet, (gpointer)pHash);
                            }
                        }
                        else
                        {
                            /* key with single value*/
                            g_hash_table_insert(pHashLast,  (gpointer)pCurXmlNode->name, strContent);
                        }
                    }
                }
            }
            else if(pParentNode && pCurXmlNode->parent && !xmlStrcmp(pCurXmlNode->parent->name, (const xmlChar*)pParentNode))
            {
                /* End tag for the item to be searched */
                if(pParentNode)
                {
                    g_free(pParentNode);
                    pParentNode = NULL;
                }
                DEBUG_CHALGROVE("\n End of Parsing %s:%d", __FUNCTION__, __LINE__);

                return;
            }

            DEBUG_CHALGROVE("\n Node type: Element, name: %s:%s", pCurXmlNode->name, pCurXmlNode->parent->name);
        }

        chalgrove_parse_xml(pXmlDoc, pCurXmlNode->children, pSearchItem, pPrefData);
    }
}


int main()
{
//  g_type_init();
    GMainLoop *mainloop = g_main_loop_new(NULL, FALSE);

    chalgrove_setmain();
    g_main_loop_run(mainloop);

    return 0;
}

